import React, {Component} from "react";
import "./Image-header.css"

class ImageBar extends Component {
    render() {
        return (
            <div className="Image-image">
                <h2 className="Image-text">
                    Maître NOM Prénom
                </h2>
            </div>
        );
    }
}

export default ImageBar;