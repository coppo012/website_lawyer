import React from 'react';
import Imageheader from './Image-header';
import {Route, Link, BrowserRouter as Router} from 'react-router-dom'
import {
    Navbar,
    NavbarBrand,
} from 'reactstrap';
import './App.css';
import Introduction from "./Introduction";
import Presentation from "./Presentation";

class App extends React.Component {
    render() {
        return (
            <Router>
                <div>
                    <Navbar className="App-header" color="dark" expand="md">
                        <NavbarBrand>
                            <Link to="/">
                                <button className="header-buttons">
                                    Accueil
                                </button>
                            </Link>
                            <Link to="/presentation">
                                <button className="header-buttons">
                                    Présentation
                                </button>
                            </Link>
                            <button className="header-buttons">
                                Où me trouver
                            </button>
                            <button className="header-buttons">
                                Contact
                            </button>

                        </NavbarBrand>
                    </Navbar>
                    <Imageheader/>
                    <Introduction/>
                    <Route path="/presentation" component={Presentation}/>
                </div>
            </Router>
        );
    }
}

export default App;
