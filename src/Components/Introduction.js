import React, {Component} from "react";
import "./Introduction.css"
import ImageBar from "./Image-header";

class Introduction extends Component {
    render() {
        return (
            <div className="introduction">
                <h1 className="introduction-text">
                    Avocat au barreau de Marseille
                </h1>
            </div>
        );
    }
}

export default Introduction;